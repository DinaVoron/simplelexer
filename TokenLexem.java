public class TokenLexem {
    Lexem lexema;
    Token token;
    public TokenLexem(Lexem lexema_, Token token_) {
        lexema = lexema_;
        token = token_;
    }

    public Lexem getLexem() {
        return lexema;
    }

    public Token getToken() {
        return token;
    }
}
